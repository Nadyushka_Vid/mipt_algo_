#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "D.h"


TEST_CASE("Test 1") {
    int sizeN = 2;
    int sizeM = 3;
    int costA = 3; 
    int costB = 2;
    std::vector<std::string> bridge;
    bridge.push_back(".**");
    bridge.push_back(".*.");

	REQUIRE(Solve(sizeN, sizeM, costA, costB, bridge) == 5);
}
TEST_CASE("Test 2") {
	int sizeN = 2;
    int sizeM = 3;
    int costA = 5; 
    int costB = 2;
    std::vector<std::string> bridge;
    bridge.push_back(".**");
    bridge.push_back(".*.");

	REQUIRE(Solve(sizeN, sizeM, costA, costB, bridge) == 6);
}
TEST_CASE("Test 3") {
	int sizeN = 2;
    int sizeM = 3;
    int costA = 3; 
    int costB = 2;
    std::vector<std::string> bridge;
    bridge.push_back(".*.");
    bridge.push_back("***");
    bridge.push_back(".*.");

	REQUIRE(Solve(sizeN, sizeM, costA, costB, bridge) == 9);
}