#include<algorithm>
#include<iostream>
#include<string>
#include<vector>

std::vector<int> GetNeighbours(int v, std::vector<std::string>& bridge, int sizeM) {
	std::vector<int> neighbours;
	int sizeN = bridge.size();
	int x = v / sizeM;
	int y = v%sizeM;
	if (x > 0 && bridge[x - 1][y] == '*') {
		neighbours.push_back(sizeM*(x - 1) + y);
	}
	if (x < sizeN - 1 && bridge[x + 1][y] == '*') {
		neighbours.push_back(sizeM*(x + 1) + y);
	}
	if (y > 0 && bridge[x][y - 1] == '*') {
		neighbours.push_back(sizeM*x + y - 1);
	}
	if (y < sizeM - 1 && bridge[x][y + 1] == '*') {
		neighbours.push_back(sizeM*x + y + 1);
	}
	return neighbours;
}

bool Kuhn(std::vector<int>& matching, std::vector<bool>& used, int v, 
	int sizeM, std::vector<std::string>& bridge) {
	if (!used[v]) {
		used[v] = true;
		std::vector<int> neighbours = GetNeighbours(v, bridge, sizeM);
		for (auto u : neighbours) {
			if (matching[u] == -1) {
				matching[u] = v;
				return true;
			}
		}
		for (auto u : neighbours) {
			if (Kuhn(matching, used, matching[u], sizeM, bridge)) {
				matching[u] = v;
				return true;
			}
		}
	}
	return false;
}

int Solve(int sizeN, int sizeM, int costA, int costB, std::vector<std::string>& bridge) {
	if (2 * costB <= costA) {
		int broken = 0;
		for (size_t i = 0; i < sizeN; i++) {
			for (size_t j = 0; j < sizeM; j++) {
				broken += (bridge[i][j] == '*') ? 1 : 0;
			}
		}
		return costB*broken;
	}
	int maxMatching = 0;
	int broken = 0;
	std::vector<int> matching(sizeN*sizeM, -1);
	std::vector<bool> used(sizeN*sizeM, false);
	for (size_t i = 0; i < sizeN*sizeM; i++) {
		int x = i / sizeM;
		int y = i%sizeM;
		if (bridge[x][y] == '*') {
			++broken;
			if (Kuhn(matching, used, i, sizeM, bridge)) {
				++maxMatching;
				used.assign(sizeN*sizeM, false);
			}
		}
	}
	maxMatching /= 2;
	return costA*maxMatching + costB*(broken - maxMatching * 2);
}