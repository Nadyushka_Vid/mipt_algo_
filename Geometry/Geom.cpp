#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "geometry.h"


TEST_CASE("Test 1: abs") {
	REQUIRE(Abs(-2.0) == 2.0);
}
TEST_CASE("Test 2") {
	CPoint p(1, 0);
    Cpoint q(2, 0);

	REQUIRE(DistBetween2CPoints(p, q) == "3 4 ");
}
TEST_CASE("Test 3") {
    CPoint p(1, 0);
    Cpoint q(2, 0);
    Cpoint r(3, 0);
	
	REQUIRE(Middle(p, r) == q);
}