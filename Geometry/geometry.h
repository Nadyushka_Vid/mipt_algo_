#define _USE_MATH_DEFINES
#include <algorithm>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<set>
#include<type_traits>
#include<vector>

const double delta = 1e-10;

double Abs(double num) {
	return (num > -delta) ? num : -num;
}

struct CPoint {
	double x = 0;
	double y = 0;

	CPoint() {}
	CPoint(double _x, double _y) :
		x(_x),
		y(_y)
	{
	}

	bool operator==(const CPoint& other) const;
	bool operator!=(const CPoint& other) const;

};

bool CPoint::operator==(const CPoint& other) const {
	return Abs(x - other.x) < delta && Abs(y - other.y) < delta;
}
bool CPoint::operator!=(const CPoint& other) const {
	return !(*this == other);
}

double DistBetween2CPoints(const CPoint& a, const CPoint& b) {
	return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}
CPoint Middle(const CPoint& a, const CPoint& b) {
	return CPoint((a.x + b.x) / 2, (a.y + b.y) / 2);
}
CPoint DevideSegment(CPoint A, CPoint B, double coefficient) {
	return CPoint((A.x + coefficient*B.x) / (1 + coefficient), (A.y + coefficient*B.y) / (1 + coefficient));
}


class CVector {
public:


	CVector(const CPoint& p, const CPoint& q) :
		x(q.x - p.x),
		y(q.y - p.y)
	{
	}
	CVector(double _x, double _y) :
		x(_x),
		y(_y)
	{
	}

	friend CPoint operator+(const CPoint& a, const CVector& v);
	friend CPoint operator-(const CPoint& a, const CVector& v);

	CVector operator-() const;

	bool operator==(const CVector& other) const;
	bool operator!=(const CVector& other) const;

	friend CVector operator+(const CVector& a, const CVector& b);
	friend CVector operator-(const CVector& a, const CVector& b);
	friend CVector operator*(const CVector& a, double k);

	CVector operator*=(double k);

	friend double DotProduct(const CVector& u, const CVector& v); // ��������� ������������
	friend double CrossProduct(const CVector& u, const CVector& v); // ��������� ������������
	CVector Rotate(double alpha) const;

private:
	double x = 0;
	double y = 0;
};

// ����������� ������� � �����
CPoint operator+(const CPoint& a, const CVector& v) {
	return CPoint(a.x + v.x, a.y + v.y);
}
CPoint operator-(const CPoint& a, const CVector& v) {
	return CPoint(a.x - v.x, a.y - v.y);
}
CVector CVector::operator-() const {
	return CVector(-x, -y);
}
bool CVector::operator==(const CVector& other) const {
	return Abs(x*other.y - y*other.x) < delta;
}
bool CVector::operator!=(const CVector& other) const {
	return !(*this == other);
}
CVector operator+(const CVector& a, const CVector& b) {
	return CVector(a.x + b.x, a.y + b.y);
}
CVector operator-(const CVector& a, const CVector& b) {
	return CVector(a.x - b.x, a.y - b.y);
}
CVector operator*(const CVector& a, double k) {
	return CVector(a.x*k, a.y*k);
}
CVector CVector::operator*=(double k) {
	*this = *this*k;
	return *this;
}
double DotProduct(const CVector& u, const CVector& v) {
	return u.x*v.x + u.y*v.y;
}
double CrossProduct(const CVector& u, const CVector& v) {
	return u.x*v.y - u.y*v.x;
}
CVector CVector::Rotate(double alpha) const { // � ��������
	double a = alpha * M_PI / 180;
	return CVector(x*cos(a) - y*sin(a), x*sin(a) + y*cos(a));
}


class CLine {
public:
	// ������� ����� �������
	CLine(const CPoint& p, const CPoint& q);
	// ������� ������������� � �������
	CLine(double k, double _b);
	// ������ � ������� �������������
	CLine(const CPoint& p, double k);
	// a, b, c
	CLine(double _a, double _b, double _c);

	bool operator==(const CLine& other) const;
	bool operator!=(const CLine& other) const;

	//����������� ������
	CPoint Intersect(const CLine& other) const;

	//������������� �� ����� �� ������
	CLine Perpendicular(const CPoint& p) const;

	//����� ������
	friend std::ostream& operator<<(std::ostream& stream, const CLine& CLine);
private:
	// ������ � ���� a*x+b*y+c=0
	double a = 0;
	double b = 0;
	double c = 0;
};
CLine::CLine(const CPoint& p, const CPoint& q) {
	a = q.y - p.y;
	b = p.x - q.x;
	c = -a*p.x - b*p.y;
}
CLine::CLine(double k, double _b) {
	a = k;
	b = -1;
	c = _b;
}
CLine::CLine(const CPoint& p, double k) {
	a = k;
	b = -1;
	c = p.y - k*p.x;
}
CLine::CLine(double _a, double _b, double _c) {
	a = _a;
	b = _b;
	c = _c;
}
bool CLine::operator==(const CLine& other) const {
	return Abs(a*other.b - b*other.a) < delta && Abs(b*other.c - c*other.b) < delta;
}
bool CLine::operator!=(const CLine& other) const {
	return !(*this == other);
}
CPoint CLine::Intersect(const CLine& other) const {
	double x = (c*other.b - other.c*b) / (other.a*b - a*other.b);
	double y = (a*other.c - c*other.a) / (b*other.a - other.b*a);
	return CPoint(x, y);
}
CLine CLine::Perpendicular(const CPoint& p) const {
	double a_ = b;
	double b_ = -a;
	double c_ = -a_*p.x - b_*p.y;
	return CLine(a_, b_, c_);
}
std::ostream& operator<<(std::ostream& stream, const CLine& CLine) {
	stream << CLine.a << "*x+" << CLine.b << "*y+" << CLine.c << "=0";
	return stream;
}


class CShape {
public:
	CShape() {}
	virtual ~CShape() {}

	virtual double Perimeter() const = 0;
	virtual double Area() const = 0;
	virtual bool operator==(const CShape& another) const = 0;
	virtual bool IsCongruentTo(const CShape& another) const = 0;
	virtual bool IsSimilarTo(const CShape& another) const = 0;
	virtual bool ContainsCPoint(CPoint CPoint) const = 0;
	virtual void Rotate(CPoint Center, double angle) = 0;
	virtual void Reflex(CPoint Center) = 0;
	virtual void Reflex(CLine axis) = 0;
	virtual void Scale(CPoint Center, double coefficient) = 0;
};

bool operator!=(const CShape& s1, const CShape& s2) {
	return !(s1 == s2);
}

class CPolygon : public CShape {
public:
	template<typename... T>
	CPolygon(const T&... CPoints) {
		PutVertex(CPoints...);
	}
	CPolygon(std::vector<CPoint> CPoints) {
		Vertices = CPoints;
	}

	size_t VerticesCount() const;
	std::vector<CPoint> GetVertices() const;
	bool IsConvex() const; // �������� ��

	double Perimeter() const override;
	double Area() const override;
	bool operator==(const CShape& another) const override;
	bool IsCongruentTo(const CShape& another) const override;
	bool IsSimilarTo(const CShape& another) const override;
	bool ContainsCPoint(CPoint CPoint) const override;
	void Rotate(CPoint Center, double angle) override;
	void Reflex(CPoint Center) override;
	void Reflex(CLine axis) override;
	void Scale(CPoint Center, double coefficient) override;

protected:
	std::vector<CPoint> Vertices;
	void PutVertex() {
	}
	/*template <typename T> void PutVertex(const T& t) {
	Vertices.push_back(t);
	}*/
	template <typename... Rest> void PutVertex(const CPoint& first, const Rest&... rest) {
		Vertices.push_back(first);
		PutVertex(rest...); // recursive call using pack expansion syntax
	}
};
size_t CPolygon::VerticesCount() const {
	return Vertices.size();
}
std::vector<CPoint> CPolygon::GetVertices() const {
	return Vertices;
}
bool CPolygon::IsConvex() const {
	if (Vertices.size() <= 2) { //����������� ������������� 
		return false;
	}

	bool ans = true;
	int prev_sign = 0;
	CVector prev_CVector(Vertices[Vertices.size() - 1], Vertices[0]);

	for (size_t i = 0; i < Vertices.size(); i++) {
		CVector cur_CVector(Vertices[i], Vertices[(i + 1) % Vertices.size()]);
		double crossPr = CrossProduct(prev_CVector, cur_CVector);
		int cur_sign = (crossPr > delta) ? 1 : -1;
		if (prev_sign != 0 && prev_sign != cur_sign) {
			ans = false;
			break;
		}
		prev_sign = cur_sign;
		prev_CVector = cur_CVector;
	}

	return ans;
}
double CPolygon::Perimeter() const {
	double Perimeter = 0;
	for (size_t i = 0; i < Vertices.size(); i++) {
		Perimeter += DistBetween2CPoints(Vertices[i], Vertices[(i + 1) % Vertices.size()]);
	}
	return Perimeter;
}
double CPolygon::Area() const {
	double Area = 0;
	for (size_t i = 1; i <= Vertices.size() - 2; i++) {
		CVector u(Vertices[0], Vertices[i]);
		CVector v(Vertices[0], Vertices[i + 1]);
		Area += CrossProduct(u, v) / 2.0;
	}
	return Abs(Area);
}
bool CPolygon::operator==(const CShape& another) const {
	if (dynamic_cast<const CPolygon*> (&another)) {
		std::vector<CPoint> otherVertices = dynamic_cast<const CPolygon*> (&another)->GetVertices();
		if (Vertices.size() != otherVertices.size()) {
			return false;
		}
		size_t startCPoint = Vertices.size();
		for (size_t i = 0; i < otherVertices.size(); i++) {
			if (otherVertices[i] == Vertices[0]) {
				startCPoint = i;
				break;
			}
		}
		if (startCPoint == Vertices.size()) {
			return false;
		}
		bool ans = true;
		for (size_t i = 0; i < Vertices.size(); i++) {
			if (Vertices[i] != otherVertices[(i + startCPoint) % otherVertices.size()]) {
				ans = false;
				break;
			}
		}
		bool ans2 = true;
		for (size_t i = 0; i < Vertices.size(); i++) {
			if (Vertices[i] != otherVertices[(startCPoint + otherVertices.size() - i) % otherVertices.size()]) {
				ans2 = false;
				break;
			}
		}
		return ans || ans2;
	}
	return false;
}
std::vector<double> GetAngles(const std::vector<CPoint>& Vertices) {
	std::vector<double> angles;
	CVector u(Vertices[0], Vertices[1]);
	CVector v(Vertices[0], Vertices[Vertices.size() - 1]);
	angles.push_back(Abs(atan2(CrossProduct(u, v), DotProduct(u, v))));
	for (size_t i = 1; i < Vertices.size(); i++) {
		u = CVector(Vertices[i], Vertices[(i + 1) % Vertices.size()]);
		v = CVector(Vertices[i], Vertices[i - 1]);
		angles.push_back(Abs(atan2(CrossProduct(u, v), DotProduct(u, v))));
	}
	return angles;
}
bool PolygonCongruentFirstDirection(std::vector<double>& angles, std::vector<double>& other_angles,
	std::vector<CPoint>& otherVertices, std::vector<CPoint> Vertices, size_t i) {
	bool AnglesSimilar = true;
	for (size_t j = 0; j < Vertices.size(); j++) {
		if (Abs(other_angles[j] - angles[(i + j) % Vertices.size()]) >= delta) {
			AnglesSimilar = false;
			break;
		}
	}
	if (AnglesSimilar) {
		bool SidesSimilar = true;
		for (size_t j = 0; j < Vertices.size(); j++) {
			if (Abs(DistBetween2CPoints(otherVertices[j], otherVertices[(j + 1) % Vertices.size()])
				- DistBetween2CPoints(Vertices[(i + j) % Vertices.size()],
					Vertices[(i + j + 1) % Vertices.size()])) >= delta) {
				SidesSimilar = false;
				break;
			}
		}
		if (SidesSimilar) {
			return true;
		}
	}
}
bool PolygonCongruentSecondDirection(std::vector<double>& angles, std::vector<double>& other_angles,
	std::vector<CPoint>& otherVertices, std::vector<CPoint> Vertices, size_t i) {
	bool AnglesSimilar = true;
	for (size_t j = 0; j < Vertices.size(); j++) {
		if (Abs(other_angles[(Vertices.size() - j) % Vertices.size()]
			- angles[(i + j) % Vertices.size()]) >= delta) {
			AnglesSimilar = false;
			break;
		}
	}
	if (AnglesSimilar) {
		bool SidesSimilar = true;
		for (size_t j = 0; j < Vertices.size(); j++) {
			if (Abs(DistBetween2CPoints(otherVertices[(Vertices.size() - j) % Vertices.size()],
				otherVertices[(Vertices.size() - j - 1) % Vertices.size()])
				- DistBetween2CPoints(Vertices[(i + j) % Vertices.size()],
					Vertices[(i + j + 1) % Vertices.size()])) >= delta) {
				SidesSimilar = false;
				break;
			}
		}
		if (SidesSimilar) {
			return true;
		}
	}
}
bool CPolygon::IsCongruentTo(const CShape& another) const {
	if (dynamic_cast<const CPolygon*> (&another)) {
		std::vector<CPoint> otherVertices = dynamic_cast<const CPolygon*> (&another)->GetVertices();
		if (Vertices.size() != otherVertices.size()) {
			return false;
		}

		std::vector<double> angles = GetAngles(Vertices);
		std::vector<double> other_angles = GetAngles(otherVertices);

		for (size_t i = 0; i < Vertices.size(); i++) {
			if (Abs(other_angles[0] - angles[i]) < delta) {
				if (PolygonCongruentFirstDirection(angles, other_angles, otherVertices, Vertices, i)) {
					return true;
				}
				//� ������ �������
				if (PolygonCongruentSecondDirection(angles, other_angles, otherVertices, Vertices, i)) {
					return true;
				}
			}
		}
		return false;
	}
	return false;
}
bool PolygonSimilarFirstDirection(std::vector<double>& angles, std::vector<double>& other_angles,
	std::vector<CPoint>& otherVertices, std::vector<CPoint> Vertices, size_t i) {
	bool AnglesSimilar = true;
	for (size_t j = 0; j < Vertices.size(); j++) {
		if (Abs(other_angles[j] - angles[(i + j) % Vertices.size()]) >= delta) {
			AnglesSimilar = false;
			break;
		}
	}
	if (AnglesSimilar) {
		bool SidesSimilar = true;
		double coefficient = DistBetween2CPoints(otherVertices[0], otherVertices[1])
			/ DistBetween2CPoints(Vertices[i], Vertices[(i + 1) % Vertices.size()]);
		for (size_t j = 0; j < Vertices.size(); j++) {
			double curCoeff = DistBetween2CPoints(otherVertices[j],
				otherVertices[(j + 1) % Vertices.size()])
				/ DistBetween2CPoints(Vertices[(i + j) % Vertices.size()],
					Vertices[(i + j + 1) % Vertices.size()]);
			if (Abs(coefficient - curCoeff) >= delta) {
				SidesSimilar = false;
				break;
			}
		}
		if (SidesSimilar) {
			return true;
		}
	}
}
bool PolygonSimilarSecondDirection(std::vector<double>& angles, std::vector<double>& other_angles,
	std::vector<CPoint>& otherVertices, std::vector<CPoint> Vertices, size_t i) {
	bool AnglesSimilar = true;
	for (size_t j = 0; j < Vertices.size(); j++) {
		if (Abs(other_angles[(Vertices.size() - j) % Vertices.size()]
			- angles[(i + j) % Vertices.size()]) >= delta) {
			AnglesSimilar = false;
			break;
		}
	}
	if (AnglesSimilar) {
		bool SidesSimilar = true;
		double coefficient = DistBetween2CPoints(otherVertices[0], otherVertices[Vertices.size() - 1])
			/ DistBetween2CPoints(Vertices[i], Vertices[(i + 1) % Vertices.size()]);
		for (size_t j = 0; j < Vertices.size(); j++) {
			double curCoeff = DistBetween2CPoints(otherVertices[(Vertices.size() - j) % Vertices.size()],
				otherVertices[(Vertices.size() - j - 1) % Vertices.size()])
				/ DistBetween2CPoints(Vertices[(i + j) % Vertices.size()],
					Vertices[(i + j + 1) % Vertices.size()]);
			if (Abs(coefficient - curCoeff) >= delta) {
				SidesSimilar = false;
				break;
			}
		}
		if (SidesSimilar) {
			return true;
		}
	}
}
bool CPolygon::IsSimilarTo(const CShape& another) const {
	if (dynamic_cast<const CPolygon*> (&another)) {
		std::vector<CPoint> otherVertices = dynamic_cast<const CPolygon*> (&another)->GetVertices();
		if (Vertices.size() != otherVertices.size()) {
			return false;
		}

		std::vector<double> angles = GetAngles(Vertices);
		std::vector<double> other_angles = GetAngles(otherVertices);

		for (size_t i = 0; i < Vertices.size(); i++) {
			if (Abs(other_angles[0] - angles[i]) < delta) {
				if (PolygonSimilarFirstDirection(angles, other_angles, otherVertices, Vertices, i)) {
					return true;
				}
				//� ������ �������
				if (PolygonSimilarSecondDirection(angles, other_angles, otherVertices, Vertices, i)) {
					return true;
				}
			}
		}
		return false;
	}
	return false;
}
bool CPolygon::ContainsCPoint(CPoint CPoint) const {
	for (size_t i = 0; i < Vertices.size(); i++) {
		if (Abs(DistBetween2CPoints(CPoint, Vertices[i]) + DistBetween2CPoints(CPoint, Vertices[(i + 1) % Vertices.size()])
			- DistBetween2CPoints(Vertices[i], Vertices[(i + 1) % Vertices.size()])) < delta) {
			return true; // ����� �� �������
		}
	}

	double angle = 0;
	for (size_t i = 0; i < Vertices.size(); i++) {
		CVector u(CPoint, Vertices[i]);
		CVector v(CPoint, Vertices[(i + 1) % Vertices.size()]);
		angle += atan2(CrossProduct(u, v), DotProduct(u, v));
	}
	return (Abs(Abs(angle) - 2 * M_PI) < delta) ? true : false;
}
void CPolygon::Rotate(CPoint Center, double angle) {
	std::vector<CPoint> NewVertices;
	for (auto Vert : Vertices) {
		CVector v(Center, Vert);
		CVector new_v = v.Rotate(angle);
		NewVertices.push_back(Center + new_v);
	}
	Vertices = NewVertices;
}
void CPolygon::Reflex(CPoint Center) {
	std::vector<CPoint> NewVertices;
	for (auto Vert : Vertices) {
		CVector v = -CVector(Center, Vert);
		NewVertices.push_back(Center + v);
	}
	Vertices = NewVertices;
}
void CPolygon::Reflex(CLine axis) {
	std::vector<CPoint> NewVertices;
	for (auto Vert : Vertices) {
		CPoint M = axis.Intersect(axis.Perpendicular(Vert));
		CVector v(Vert, M);
		NewVertices.push_back(M + v);
	}
	Vertices = NewVertices;
}
void CPolygon::Scale(CPoint Center, double coefficient) {
	std::vector<CPoint> NewVertices;
	for (auto Vert : Vertices) {
		CVector v = CVector(Center, Vert) * coefficient;
		NewVertices.push_back(Center + v);
	}
	Vertices = NewVertices;
}




class CEllipse : public CShape {
public:
	CEllipse() {}
	CEllipse(CPoint f1, CPoint f2, double sum) :
		F1(f1),
		F2(f2),
		a(sum / 2)
	{
	}

	std::pair<CPoint, CPoint> Focuses() const;
	std::pair<CLine, CLine> Directrices() const;
	double Eccentricity() const;
	CPoint Center() const;

	double GetB() const;

	double Perimeter() const override;
	double Area() const override;
	bool operator==(const CShape& another) const override;
	bool IsCongruentTo(const CShape& another) const override;
	bool IsSimilarTo(const CShape& another) const override;
	bool ContainsCPoint(CPoint CPoint) const override;
	void Rotate(CPoint Center, double angle) override;
	void Reflex(CPoint Center) override;
	void Reflex(CLine axis) override;
	void Scale(CPoint Center, double coefficient) override;
protected:
	//��� ������ � ������� �������
	CPoint F1 = CPoint(0, 0);
	CPoint F2 = CPoint(0, 0);
	double a = 0;
};
std::pair<CPoint, CPoint> CEllipse::Focuses() const {
	return std::make_pair(F1, F2);
}
std::pair<CLine, CLine> CEllipse::Directrices() const {
	double dist_from_O_to_directrice = a / Eccentricity();
	CPoint O = Center();
	CVector Od1 = CVector(O, F1) * (dist_from_O_to_directrice / DistBetween2CPoints(O, F1));
	CLine d1 = CLine(F1, F2).Perpendicular(O + Od1);
	CVector Od2 = CVector(O, F2) * (dist_from_O_to_directrice / DistBetween2CPoints(O, F2));
	CLine d2 = CLine(F1, F2).Perpendicular(O + Od2);
	return std::make_pair(d1, d2);
}
double CEllipse::Eccentricity() const {
	return DistBetween2CPoints(F1, F2) / (2 * a);
}
CPoint CEllipse::Center() const {
	return Middle(F1, F2);
}
double CEllipse::GetB() const {
	return a*sqrt(1 - Eccentricity()*Eccentricity());
}
double CEllipse::Perimeter() const {
	double b = GetB();
	return M_PI*(3 * (a + b) - sqrt((3 * a + b)*(a + 3 * b)));
}
double CEllipse::Area() const {
	return M_PI*a*GetB();
}
bool CEllipse::operator==(const CShape& another) const {
	if (dynamic_cast<const CEllipse*> (&another)) {
		CPoint otherF1 = dynamic_cast<const CEllipse*> (&another)->F1;
		CPoint otherF2 = dynamic_cast<const CEllipse*> (&another)->F2;
		double other_a = dynamic_cast<const CEllipse*> (&another)->a;
		return (F1 == otherF1) && (F2 == otherF2) && (Abs(a - other_a) < delta);
	}
	else {
		return false;
	}
}
bool CEllipse::IsCongruentTo(const CShape& another) const {
	if (dynamic_cast<const CEllipse*> (&another)) {
		double other_b = dynamic_cast<const CEllipse*> (&another)->GetB();
		double other_a = dynamic_cast<const CEllipse*> (&another)->a;
		double b = GetB();
		return (Abs(b - other_b) < delta) && (Abs(a - other_a) < delta);
	}
	else {
		return false;
	}
}
bool CEllipse::IsSimilarTo(const CShape& another) const {
	if (dynamic_cast<const CEllipse*> (&another)) {
		double other_b = dynamic_cast<const CEllipse*> (&another)->GetB();
		double other_a = dynamic_cast<const CEllipse*> (&another)->a;
		double b = GetB();
		double k1 = a / other_a;
		double k2 = b / other_b;
		return Abs(k1 - k2) < delta;
	}
	else {
		return false;
	}
}
bool CEllipse::ContainsCPoint(CPoint CPoint) const {
	return DistBetween2CPoints(F1, CPoint) + DistBetween2CPoints(F2, CPoint) - 2 * a < -delta;
}
void CEllipse::Rotate(CPoint Center, double angle) {
	CVector CF1 = CVector(Center, F1).Rotate(angle);
	CVector CF2 = CVector(Center, F2).Rotate(angle);
	F1 = Center + CF1;
	F2 = Center + CF2;
}
void CEllipse::Reflex(CPoint Center) {
	CVector CF1 = -CVector(Center, F1);
	CVector CF2 = -CVector(Center, F2);
	F1 = Center + CF1;
	F2 = Center + CF2;
}
void CEllipse::Reflex(CLine axis) {
	CPoint M1 = axis.Intersect(axis.Perpendicular(F1));
	CPoint M2 = axis.Intersect(axis.Perpendicular(F2));
	CVector v1(F1, M1);
	CVector v2(F2, M2);
	F1 = M1 + v1;
	F2 = M2 + v2;
}
void CEllipse::Scale(CPoint Center, double coefficient) {
	CVector CF1 = CVector(Center, F1)*coefficient;
	CVector CF2 = CVector(Center, F2)*coefficient;
	F1 = Center + CF1;
	F2 = Center + CF2;
	a *= Abs(coefficient);
}



class CCircle : public CEllipse {
public:
	CCircle(CPoint O, double r) {
		RadiusAtr = r;
		CenterAtr = O;
		CEllipse::F1 = O;
		CEllipse::F2 = O;
		CEllipse::a = r;
	}

	double Radius() const;

	friend std::ostream& operator<<(std::ostream& stream, const CCircle& c);
private:
	double RadiusAtr = 0;
	CPoint CenterAtr;
};
double CCircle::Radius() const {
	return RadiusAtr;
}
std::ostream& operator<<(std::ostream& stream, const CCircle& c) {
	stream << "(" << c.CenterAtr.x << ", " << c.CenterAtr.y << ")  r=" << c.RadiusAtr;
	return stream;
}


class CRectangle : public CPolygon {
public:
	CRectangle() {}
	CRectangle(const CPoint& _A, const CPoint& _C, double k) {
		A = _A;
		C = _C;
		double alpha = atan(k) * 180 / M_PI;
		CVector AC(A, C);
		CVector CA(C, A);
		CLine l1 = CLine(A, A + AC.Rotate(std::max(alpha, 90 - alpha)));
		CLine l2 = CLine(C, C + CA.Rotate(-std::min(alpha, 90 - alpha)));
		B = l1.Intersect(l2);
		D = A + CVector(B, C);
		CPolygon::Vertices.push_back(A);
		CPolygon::Vertices.push_back(B);
		CPolygon::Vertices.push_back(C);
		CPolygon::Vertices.push_back(D);
	}

	CPoint Center() const;
	std::pair<CLine, CLine> diagonals() const;
protected:
	CPoint A;
	CPoint B;
	CPoint C;
	CPoint D;
};
CPoint CRectangle::Center() const {
	return Middle(A, C);
}
std::pair<CLine, CLine> CRectangle::diagonals() const {
	return std::make_pair(CLine(A, C), CLine(B, D));
}


class CSquare : public CRectangle {
public:
	CSquare(CPoint A, CPoint C) {
		CPoint O = Middle(A, C);
		CPoint B = O + CVector(O, A).Rotate(90);
		CPoint D = O + CVector(O, A).Rotate(-90);
		CPolygon::Vertices.push_back(A);
		CPolygon::Vertices.push_back(B);
		CPolygon::Vertices.push_back(C);
		CPolygon::Vertices.push_back(D);
	}

	CCircle CircumscribedCCircle() const;
	CCircle InscribedCCircle() const;
};
CCircle CSquare::CircumscribedCCircle() const {
	return CCircle(Middle(CPolygon::Vertices[0], CPolygon::Vertices[2]),
		DistBetween2CPoints(CPolygon::Vertices[0], CPolygon::Vertices[2]) / 2);
}
CCircle CSquare::InscribedCCircle() const {
	CLine AB = CLine(CPolygon::Vertices[0], CPolygon::Vertices[1]);
	CPoint O = Middle(CPolygon::Vertices[0], CPolygon::Vertices[2]);
	CLine L = AB.Perpendicular(O);
	CPoint P = L.Intersect(AB);
	return CCircle(O, DistBetween2CPoints(O, P));
}


class CTriangle : public CPolygon {
public:
	CTriangle(CPoint _A, CPoint _B, CPoint _C) {
		A = _A;
		B = _B;
		C = _C;
		CPolygon::Vertices.push_back(A);
		CPolygon::Vertices.push_back(B);
		CPolygon::Vertices.push_back(C);
	}

	CCircle CircumscribedCCircle() const;
	CCircle InscribedCCircle() const;
	CPoint Centroid() const;
	CPoint OrthoCenter() const;
	CLine EulerCLine() const;
	CCircle NineCPointsCCircle() const;
private:
	CPoint A;
	CPoint B;
	CPoint C;
};
CCircle CTriangle::CircumscribedCCircle() const {
	CLine L1 = CLine(A, B).Perpendicular(Middle(A, B));
	CLine L2 = CLine(B, C).Perpendicular(Middle(B, C));
	CPoint O = L1.Intersect(L2);
	return CCircle(O, DistBetween2CPoints(O, A));
}
CCircle CTriangle::InscribedCCircle() const {
	double AB = DistBetween2CPoints(A, B);
	double BC = DistBetween2CPoints(C, B);
	double AC = DistBetween2CPoints(A, C);
	CPoint D = DevideSegment(A, C, AB / BC);
	CPoint E = DevideSegment(B, A, BC / AC);
	CPoint O = CLine(B, D).Intersect(CLine(C, E));
	CPoint H = CLine(A, B).Intersect(CLine(A, B).Perpendicular(O));
	return CCircle(O, DistBetween2CPoints(O, H));
}
CPoint CTriangle::Centroid() const {
	CPoint M = Middle(B, C);
	CPoint N = Middle(A, C);
	return CLine(A, M).Intersect(CLine(B, N));
}
CPoint CTriangle::OrthoCenter() const {
	CLine h1 = CLine(A, B).Perpendicular(C);
	CLine h2 = CLine(B, C).Perpendicular(A);
	return h1.Intersect(h2);
}
CLine CTriangle::EulerCLine() const {
	return CLine(CircumscribedCCircle().Center(), OrthoCenter());
}
CCircle CTriangle::NineCPointsCCircle() const {
	CTriangle MNK(Middle(A, B), Middle(B, C), Middle(A, C));
	return MNK.CircumscribedCCircle();
}