#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "D.h"
#include<string>


TEST_CASE("Test 1") {
	int numVertices = 3;
	std::vector<int> parent(numVertices - 1);
    parent[0] = 0;
    parent[1] = 1;
	std::vector<std::vector<int>> graph(numVertices);
	for (size_t i = 0; i < numVertices - 1; i++) {
		graph[parent[i]].push_back(i + 1);
	}
	std::vector<int> height(numVertices, 0);
	DFS(0, graph, height);
	std::vector<int> log = GetLog(numVertices);
	std::vector<std::vector<int>> func = GetFunc(numVertices, height, parent, log);

	REQUIRE(LCA(0, 1, height, func, 1) == 0);
}

TEST_CASE("Test 2") {
	int numVertices = 3;
	std::vector<int> parent(numVertices - 1);
    parent[0] = 0;
    parent[1] = 1;
	std::vector<std::vector<int>> graph(numVertices);
	for (size_t i = 0; i < numVertices - 1; i++) {
		graph[parent[i]].push_back(i + 1);
	}
	std::vector<int> height(numVertices, 0);
	DFS(0, graph, height);
	std::vector<int> log = GetLog(numVertices);
	std::vector<std::vector<int>> func = GetFunc(numVertices, height, parent, log);

	REQUIRE(LCA(2, 1, height, func, 1) == 1);
}