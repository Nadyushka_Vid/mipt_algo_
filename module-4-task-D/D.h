#include<algorithm>
#include<iostream>
#include<vector>

void DFS(int v, std::vector<std::vector<int>>& graph, std::vector<int>& height) {
	for (auto u : graph[v]) {
		height[u] = height[v] + 1;
		DFS(u, graph, height);
	}
}
std::vector<int> GetLog(int n) {
	std::vector<int> log(n + 1, 0);
	for (size_t i = 2; i < n + 1; i++) {
		log[i] = log[i >> 1] + 1;
	}
	return log;
}
std::vector<std::vector<int>> GetFunc(int numVertices, std::vector<int>& height, std::vector<int>& parent, std::vector<int>& log) {
	std::vector<std::vector<int>> func(numVertices, std::vector<int>(log[numVertices] + 1, 0));
	for (size_t i = 0; i < numVertices - 1; i++)
	{
		func[i + 1][0] = parent[i];
	}
	std::vector<std::pair<int, int>> order;
	for (size_t i = 0; i < numVertices; i++) {
		order.push_back(std::make_pair(height[i], i));
	}
	std::sort(order.begin(), order.end());
	for (auto v : order) {
		for (size_t i = 1; i <= log[v.first]; i++) {
			func[v.second][i] = func[func[v.second][i - 1]][i - 1];
		}
	}
	return func;
}
int LCA(int v, int u, std::vector<int>& height, std::vector<std::vector<int>>& func, int logN) {
	if (height[u] > height[v]) {
		std::swap(u, v);
	}
	int delta = height[v] - height[u];
	int curPower = 0;
	while (delta > 0) {
		if (delta % 2) {
			v = func[v][curPower];
		}
		delta >>= 1;
		++curPower;
	}
	if (v == u) {
		return v;
	}
	for (int i = logN - 1; i >= 0; i--) {
		int v_ = func[v][i];
		int u_ = func[u][i];
		if (v_ != u_) {
			v = v_;
			u = u_;
		}
	}
	return func[v][0];
}
long long Solve(int numVertices, long long numRequests, std::vector<int>& height,
	std::vector<int>& log, std::vector<std::vector<int>>& func) {
	long long a1 = 0;
	long long a2 = 0;
	long long x = 0;
	long long y = 0;
	long long z = 0;
	std::cin >> a1 >> a2 >> x >> y >> z;
	/*x %= numVertices;
	y %= numVertices;
	z %= numVertices;*/
	long long prev_v = LCA(a1, a2, height, func, log[numVertices]);
	long long ans = prev_v;
	for (long long i = 0; i < numRequests - 1; i++) {
		long long a3 = (x * a1 + y * a2 + z) % numVertices;
		long long a4 = (x * a2 + y * a3 + z) % numVertices;
		a1 = a3, a2 = a4;
		prev_v = LCA((a1 + prev_v) % numVertices, a2, height, func, log[numVertices]);
		ans += prev_v;
	}
	return ans;
}