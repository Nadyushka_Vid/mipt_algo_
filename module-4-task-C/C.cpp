#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "C.h"
#include<string>


TEST_CASE("Test 1") {
	int length = 4;
	std::vector<std::pair<int, int>> tree(4 * length + 1, std::make_pair(1e3, 1e3));
	std::vector<int> colour(length, 0);
    colour[0] = 100;
    colour[1] = 10;
    colour[2] = 20;
    colour[3] = 30;
	Build(1, 0, length - 1, colour, tree);
    int lChange = 0;
	int rChange = 1;
	int r = 50;
	int g = 0;
	int b = 0;
	int lGet = 1;
	int rGet = 2;
	Update(1, 0, length - 1, lChange, rChange, r + g + b, tree);

	REQUIRE(Get(1, 0, length - 1, lGet, rGet, tree) == 20);
}

TEST_CASE("Test 2") {
	int length = 3;
	std::vector<std::pair<int, int>> tree(4 * length + 1, std::make_pair(1e3, 1e3));
	std::vector<int> colour(length, 0);
    colour[0] = 10;
    colour[1] = 10;
    colour[2] = 10;
	Build(1, 0, length - 1, colour, tree);
    int lChange = 0;
	int rChange = 2;
	int r = 2;
	int g = 0;
	int b = 0;
	int lGet = 0;
	int rGet = 2;
	Update(1, 0, length - 1, lChange, rChange, r + g + b, tree);

	REQUIRE(Get(1, 0, length - 1, lGet, rGet, tree) == 2);
}