#include<algorithm>
#include<iostream>
#include<vector>


void Build(int v, int l, int r, std::vector<int>& colour, std::vector<std::pair<int, int>>& tree) {
	if (l == r) {
		tree[v].first = colour[l];
	}
	else {
		int m = (l + r) / 2;
		Build(v * 2, l, m, colour, tree);
		Build(v * 2 + 1, m + 1, r, colour, tree);
		tree[v] = std::min(tree[v * 2], tree[v * 2 + 1]);
	}
}
void Push(int v, std::vector<std::pair<int, int>>& tree) {
	if (tree[v].second != 1e3) {
		tree[v * 2].second = tree[v * 2 + 1].second = tree[v].second;
		tree[v * 2].first = tree[v * 2 + 1].first = tree[v].second;
		tree[v].second = 1e3;
	}
}
void Update(int v, int tl, int tr, int l, int r, int colour, std::vector<std::pair<int, int>>& tree) {
	if (l > r) {
		return;
	}
	if (l == tl && tr == r) {
		tree[v].second = colour;
		tree[v].first = colour;
	}
	else {
		Push(v, tree);
		int tm = (tl + tr) / 2;
		Update(v * 2, tl, tm, l, std::min(r, tm), colour, tree);
		Update(v * 2 + 1, tm + 1, tr, std::max(l, tm + 1), r, colour, tree);
		tree[v].first = std::min(tree[v * 2].first, tree[v * 2 + 1].first);
	}
}
int Get(int v, int tl, int tr, int l, int r, std::vector<std::pair<int, int>>& tree) {
	if (tr < l || tl > r) {
		return 1e3;
	}
	if (tl >= l && tr <= r) {
		return tree[v].first;
	}
	Push(v, tree);
	int tm = (tl + tr) / 2;
	return std::min(Get(v * 2, tl, tm, l, std::min(r, tm), tree),
		Get(v * 2 + 1, tm + 1, tr, std::max(l, tm + 1), r, tree));
}
int main() {
	int length = 0;
	std::cin >> length;
	std::vector<std::pair<int, int>> tree(4 * length + 1, std::make_pair(1e3, 1e3));
	std::vector<int> colour(length, 0);
	for (size_t i = 0; i < length; i++) {
		int r = 0;
		int g = 0;
		int b = 0;
		std::cin >> r >> g >> b;
		colour[i] = r + g + b;
	}
	Build(1, 0, length - 1, colour, tree);
	int numRequests = 0;
	std::cin >> numRequests;
	for (size_t i = 0; i < numRequests; i++) {
		int lChange = 0;
		int rChange = 0;
		int r = 0;
		int g = 0;
		int b = 0;
		int lGet = 0;
		int rGet = 0;
		std::cin >> lChange >> rChange >> r >> g >> b >> lGet >> rGet;
		Update(1, 0, length - 1, lChange, rChange, r + g + b, tree);
		std::cout << Get(1, 0, length - 1, lGet, rGet, tree) << " ";
	}
	system("pause");
	return 0;
}