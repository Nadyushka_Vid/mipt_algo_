#include<algorithm>
#include<iostream>
#include<vector>

struct CEdge {
	long long weight;
	int start;
	int finish;
	CEdge(long long _weight, int _start, int _finish) :
		start(_start),
		finish(_finish),
		weight(_weight)
	{
	}
};

bool operator<(CEdge e1, CEdge e2) {
	return e1.weight < e2.weight;
}

int FindSet(int v, std::vector<int>& comp) {
	if (comp[v] == v) {
		return v;
	}
	int u = FindSet(comp[v], comp);
	comp[v] = u;
	return u;
}

void Union(int v, int u, std::vector<int>& comp) {
	if (rand() % 2) {
		comp[v] = u;
	}
	else {
		comp[u] = v;
	}
}

long long MSTWeight(std::vector<CEdge>& edges, int numVertices) {
	long long ans = 0;
	std::vector<int> comp(numVertices);
	for (size_t i = 0; i < numVertices; i++) {
		comp[i] = i;
	}
	std::sort(edges.begin(), edges.end());
	for (auto e : edges) {
		if (FindSet(e.start, comp) != FindSet(e.finish, comp)) {
			ans += e.weight;
			Union(FindSet(e.start, comp), FindSet(e.finish, comp), comp);
		}
	}
	return ans;
}
int main() {
	int numVertices = 0;
	int numSpecialOffers = 0;
	std::cin >> numVertices >> numSpecialOffers;
	std::vector<CEdge> edges;
	std::vector<long long> verticeCost(numVertices);
	size_t minVert = 0;
	for (size_t i = 0; i < numVertices; i++) {
		std::cin >> verticeCost[i];
		if (verticeCost[i] < verticeCost[minVert]) {
			minVert = i;
		}
	}
	for (size_t i = 0; i < numVertices; i++) {
		if (i != minVert) {
			edges.push_back(CEdge(verticeCost[i] + verticeCost[minVert], i, minVert));
		}
	}
	for (size_t i = 0; i < numSpecialOffers; i++) {
		int b = 0;
		int e = 0;
		long long w = 0;
		std::cin >> b >> e >> w;
		edges.push_back(CEdge(w, b - 1, e - 1));
	}
	std::cout << MSTWeight(edges, numVertices);
	system("pause");
	return 0;
}