#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "C.h"


TEST_CASE("Test 1: 1 edge") {
	int numVertices = 2;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(1,1,2));
	edges.push_back(CEdge(2,0,2));

	REQUIRE(MSTWeight(edges, numVertices) == 3);
}
TEST_CASE("Test 2: simple cycle") {
	int numVertices = 3;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(1,1,0));
	edges.push_back(CEdge(2,0,0));
	edges.push_back(CEdge(1,2,0));
	edges.push_back(CEdge(3,0,0));
	edges.push_back(CEdge(2,2,0));
	edges.push_back(CEdge(3,1,0));

	REQUIRE(MSTWeight(edges, numVertices) == 2);
}
TEST_CASE("Test 3: sth else") {
	int numVertices = 3;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(3,0,1));
	edges.push_back(CEdge(3,1,0));
	edges.push_back(CEdge(1,0,1));
	edges.push_back(CEdge(1,1,0));
	edges.push_back(CEdge(2,0,2));
	edges.push_back(CEdge(2,2,0));
	edges.push_back(CEdge(4,1,2));
	edges.push_back(CEdge(4,2,1));

	REQUIRE(MSTWeight(edges, numVertices) == 3);
}