#include<algorithm>
#include<iostream>
#include<vector>

struct CEdge {
	long long weight;
	int start;
	int finish;
	CEdge(long long _weight, int _start, int _finish) :
		start(_start),
		finish(_finish),
		weight(_weight)
	{
	}
};

bool operator<(CEdge e1, CEdge e2) {
	return e1.weight < e2.weight;
}

int FindSet(int v, std::vector<int>& comp) {
	if (comp[v] == v) {
		return v;
	}
	int u = FindSet(comp[v], comp);
	comp[v] = u;
	return u;
}

void Union(int v, int u, std::vector<int>& comp) {
	if (rand() % 2) {
		comp[v] = u;
	}
	else {
		comp[u] = v;
	}
}

long long MSTWeight(std::vector<CEdge>& edges, int numVertices) {
	long long ans = 0;
	std::vector<int> comp(numVertices);
	for (size_t i = 0; i < numVertices; i++) {
		comp[i] = i;
	}
	std::sort(edges.begin(), edges.end());
	for (auto e : edges) {
		if (FindSet(e.start, comp) != FindSet(e.finish, comp)) {
			ans += e.weight;
			Union(FindSet(e.start, comp), FindSet(e.finish, comp), comp);
		}
	}
	return ans;
}