#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "E.h"
#include<string>


TEST_CASE("Test 1") {
	CTreap t;
    t.InsertAt(0, "myau");
    t.InsertAt(0, "krya");

	REQUIRE(t.GetAt(0) == "krya");
}

TEST_CASE("Test 2") {
	CTreap t;
    t.InsertAt(0, "myau");
    t.InsertAt(0, "krya");
    t.InsertAt(2, "gav");
    t.DeleteAt(1);

	REQUIRE(t.GetAt(1) == "gav");
}