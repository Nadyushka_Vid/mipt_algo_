#include<algorithm>
#include<iostream>
#include<string>
#include<vector>

struct CNode {
	int size = 1;
	int y = std::rand();
	CNode* left = nullptr;
	CNode* right = nullptr;
	std::string str = "";
	CNode(std::string value) {
		str = value;
	}
};

int GetSize(CNode* node) {
	return (node == nullptr) ? 0 : node->size;
}

void Update(CNode* node) {
	if (node != nullptr) {
		node->size = 1 + GetSize(node->left) + GetSize(node->right);
	}
}
std::pair<CNode*, CNode*> Split(CNode* node, int key) {
	if (node == nullptr) {
		return std::make_pair(nullptr, nullptr);
	}
	int l = GetSize(node->left);
	if (l >= key) {
		std::pair<CNode*, CNode*> pair = Split(node->left, key);
		node->left = pair.second;
		Update(node->left);
		Update(node);
		return std::make_pair(pair.first, node);
	}
	else {
		std::pair<CNode*, CNode*> pair = Split(node->right, key - l - 1);
		node->right = pair.first;
		Update(node->right);
		Update(node);
		return std::make_pair(node, pair.second);
	}
}
CNode* Merge(CNode* node1, CNode* node2) {
	if (node2 == nullptr) {
		return node1;
	}
	if (node1 == nullptr) {
		return node2;
	}
	if (node1->y > node2->y) {
		node1->right = Merge(node1->right, node2);
		Update(node1->right);
		Update(node1);
		return node1;
	}
	else {
		node2->left = Merge(node1, node2->left);
		Update(node2->left);
		Update(node2);
		return node2;
	}
}
std::string Find(CNode* node, int key) {
	if (GetSize(node->left) == key) {
		return node->str;
	}
	if (key < GetSize(node->left)) {
		return Find(node->left, key);
	}
	else {
		return Find(node->right, key - GetSize(node->left) - 1);
	}
}
class CTreap {
public:
	void InsertAt(int position, const std::string& value) {
		CNode* newNode = new CNode(value);
		if (root == nullptr) {
			root = newNode;
		}
		else {
			std::pair<CNode*, CNode*> pair = Split(root, position);
			pair.first = Merge(pair.first, newNode);
			root = Merge(pair.first, pair.second);
		}
	}
	void DeleteAt(int position) {
		std::pair<CNode*, CNode*> pair = Split(root, position + 1);
		std::pair<CNode*, CNode*> pair2 = Split(pair.first, position);
		root = Merge(pair2.first, pair.second);
	}
	std::string GetAt(int position) {
		return Find(root, position);
	}
private:
	CNode* root = nullptr;
};