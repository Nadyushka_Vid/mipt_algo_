#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "A.h"


TEST_CASE("Test 1: 1 edge") {
	int numVertices = 2;
	std::vector<std::vector<std::pair<int, int>>> graph(numVertices + 1);
	graph[1].push_back(std::make_pair(2, 3));
	graph[2].push_back(std::make_pair(1, 3));

	REQUIRE(MSTWeight(graph, numVertices) == 3);
}
TEST_CASE("Test 2: simple cycle") {
	int numVertices = 3;
	std::vector<std::vector<std::pair<int, int>>> graph(numVertices + 1);
	graph[1].push_back(std::make_pair(2, 1));
	graph[2].push_back(std::make_pair(1, 1));
	graph[1].push_back(std::make_pair(3, 1));
	graph[3].push_back(std::make_pair(1, 1));
	graph[3].push_back(std::make_pair(2, 1));
	graph[2].push_back(std::make_pair(3, 1));

	REQUIRE(MSTWeight(graph, numVertices) == 2);
}
TEST_CASE("Test 3: sth else") {
	int numVertices = 3;
	std::vector<std::vector<std::pair<int, int>>> graph(numVertices + 1);
	graph[1].push_back(std::make_pair(2, 3));
	graph[2].push_back(std::make_pair(1, 3));
	graph[1].push_back(std::make_pair(2, 1));
	graph[2].push_back(std::make_pair(1, 1));
	graph[1].push_back(std::make_pair(3, 2));
	graph[3].push_back(std::make_pair(1, 2));
	graph[3].push_back(std::make_pair(2, 4));
	graph[2].push_back(std::make_pair(3, 4));

	REQUIRE(MSTWeight(graph, numVertices) == 3);
}