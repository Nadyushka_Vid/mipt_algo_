#include<algorithm>
#include<iostream>
#include<vector>

int MSTWeight(std::vector<std::vector<std::pair<int, int>>>& graph, int numVertices) {
	std::vector<bool> used(numVertices + 1, false);
	used[1] = true;
	std::vector<int> dist(numVertices + 1, 1e9);
	dist[1] = 0;
	int ans = 0;
	for (auto i : graph[1]) {
		dist[i.first] = std::min(dist[i.first], i.second);
	}
	for (size_t i = 0; i < numVertices - 1; i++) {
		int vertice_with_min_dist = -1;
		for (size_t j = 1; j <= numVertices; j++) {
			if (!used[j] && (vertice_with_min_dist == -1 || dist[j] < dist[vertice_with_min_dist])) {
				vertice_with_min_dist = j;
			}
		}
		ans += dist[vertice_with_min_dist];
		used[vertice_with_min_dist] = true;
		for (auto i : graph[vertice_with_min_dist]) {
			dist[i.first] = std::min(dist[i.first], i.second);
		}
	}
	return ans;
}
int main() {
	int numVertices = 0;
	int numEdges = 0;
	std::cin >> numVertices >> numEdges;
	std::vector<std::vector<std::pair<int, int>>> graph(numVertices + 1);
	for (size_t i = 0; i < numEdges; i++) {
		int b = 0;
		int e = 0;
		int w = 0;
		std::cin >> b >> e >> w;
		graph[b].push_back(std::make_pair(e, w));
		graph[e].push_back(std::make_pair(b, w));
	}
	std::cout << MSTWeight(graph, numVertices);
	system("pause");
	return 0;
}