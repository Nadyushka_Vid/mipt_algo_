#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "B.h"
#include<string>


TEST_CASE("Test 1") {
	int numStations = 6;
	std::vector<CNode> tree(4 * numStations + 1);
	std::vector<int> startState(numStations - 1);
    startState[0] = 1;
    startState[1] = 0;
    startState[2] = 0;
    startState[3] = 0;
    startState[4] = 1;
	int capacity = 5;
	Build(1, 1, numStations, startState, capacity, tree);
    std::string res = ""

    int l=1,r=6,add=4;
    int min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "0 ";
	}
	
    l=1,r=4,add=2;
    min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "1 ";
	}

    l=1,r=2,add=1;
    min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "2 ";
	}

	REQUIRE(result == "1 2 ");
}

TEST_CASE("Test 2") {
	int numStations = 6;
	std::vector<CNode> tree(4 * numStations + 1);
	std::vector<int> startState(numStations - 1);
    startState[0] = 0;
    startState[1] = 0;
    startState[2] = 0;
    startState[3] = 0;
    startState[4] = 0;
	int capacity = 1;
	Build(1, 1, numStations, startState, capacity, tree);
    std::string res = ""

    int l=1,r=2,add=1;
    int min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "0 ";
	}
	
    l=2,r=3,add=1;
    min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "1 ";
	}

    l=1,r=3,add=1;
    min = Get(1, 1, numStations, l, r, 0, tree);
    if (min >= add) {
			Update(1, 1, numStations, l, r, -add, tree);
		}
	else {
			result += "2 ";
	}

	REQUIRE(result == "1 ");
}