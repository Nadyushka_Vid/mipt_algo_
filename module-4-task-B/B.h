#include<algorithm>
#include<iostream>
#include<vector>


struct CNode {
	int mn, add;
};
void Build(int v, int l, int r, std::vector<int>& startState, int cap, std::vector<CNode>& tree) {
	if (l + 1 == r) {
		tree[v].mn = cap - startState[l - 1];
	}
	else {
		int m = (l + r) / 2;
		Build(v * 2, l, m, startState, cap, tree);
		Build(v * 2 + 1, m, r, startState, cap, tree);
		tree[v].mn = std::min(tree[v * 2].mn, tree[v * 2 + 1].mn);
	}
}
void Update(int v, int l, int r, int a, int b, int add, std::vector<CNode>& tree) {
	if (r <= a || l >= b)
		return;
	if (l >= a && r <= b) {
		tree[v].add += add;
		return;
	}
	else {
		int m = (l + r) / 2;
		Update(v * 2, l, m, a, b, add, tree);
		Update(v * 2 + 1, m, r, a, b, add, tree);
	}
	tree[v].mn = std::min(tree[v * 2].mn + tree[v * 2].add, tree[v * 2 + 1].mn + tree[v * 2 + 1].add);
}
int Get(int v, int l, int r, int a, int b, int d, std::vector<CNode>& tree) {
	if (r <= a || l >= b)
		return 1e7;
	if (l >= a && r <= b) {
		return tree[v].mn + tree[v].add + d;
	}
	int m = (l + r) / 2;
	return std::min(Get(v * 2, l, m, a, b, d + tree[v].add, tree),
		Get(v * 2 + 1, m, r, a, b, d + tree[v].add, tree));
}