#include<algorithm>
#include<iostream>
#include<set>
#include<vector>

std::vector<int> GetLog(int n) {
	std::vector<int> log(n + 1, 0);
	for (size_t i = 2; i < n + 1; i++) {
		log[i] = log[i >> 1] + 1;
	}
	return log;
}
void CountSparseTable(std::vector<std::vector<std::pair<int, int>>>& SparseTable, 
	std::vector<int>& elem, int n, std::vector<int>& log) {
	for (size_t i = 0; i < n; i++) {
		SparseTable[i][0] = std::make_pair(i, i);
	}
	for (size_t i = 0; i < n - 1; i++) {
		if (elem[i] <= elem[i + 1]) {

		}
		SparseTable[i][1] = (elem[i] <= elem[i + 1]) ? std::make_pair(i, i + 1) : std::make_pair(i + 1, i);
	}
	for (size_t j = 2; j <= log[n]; j++) {
		for (size_t i = 0; i < n; i++)
		{
			std::set<std::pair<int, int>> sortedMins;
			sortedMins.insert(std::make_pair(elem[SparseTable[i][j - 1].first], SparseTable[i][j - 1].first));
			sortedMins.insert(std::make_pair(elem[SparseTable[i][j - 1].second], SparseTable[i][j - 1].second));
			if (i + (1 << (j - 1)) >= n) {
				continue;
				/**/
			}
			sortedMins.insert(std::make_pair(elem[SparseTable[i + (1 << (j - 1))][j - 1].first], 
				SparseTable[i + (1 << (j - 1))][j - 1].first));
			sortedMins.insert(std::make_pair(elem[SparseTable[i + (1 << (j - 1))][j - 1].second],
				SparseTable[i + (1 << (j - 1))][j - 1].second));
			auto min = sortedMins.begin();
			SparseTable[i][j].first = (*min).second;
			++min;
			SparseTable[i][j].second = (*min).second;
		}
	}
}
int GetAns(int l, int r, std::vector<std::vector<std::pair<int, int>>>& SparseTable, 
	std::vector<int>& log, std::vector<int>& elem) {
	std::set<std::pair<int, int>> sortedMins;
	sortedMins.insert(std::make_pair(elem[SparseTable[l][log[r - l + 1]].first], SparseTable[l][log[r - l + 1]].first));
	sortedMins.insert(std::make_pair(elem[SparseTable[l][log[r - l + 1]].second], SparseTable[l][log[r - l + 1]].second));
	sortedMins.insert(std::make_pair(elem[SparseTable[r - (1 << log[r - l + 1]) + 1][log[r - l + 1]].first],
		SparseTable[r - (1 << log[r - l + 1]) + 1][log[r - l + 1]].first));
	sortedMins.insert(std::make_pair(elem[SparseTable[r - (1 << log[r - l + 1]) + 1][log[r - l + 1]].second],
		SparseTable[r - (1 << log[r - l + 1]) + 1][log[r - l + 1]].second));
	auto secondMin = sortedMins.begin();
	++secondMin;
	return (*secondMin).first;
}
int main() {
	int n = 0;
	int numRequests = 0;
	std::cin >> n >> numRequests;
	std::vector<int> elem(n + 1, 1e7);
	for (size_t i = 0; i < n; i++) {
		std::cin >> elem[i];
	}
	std::vector<int> log = GetLog(n);
	std::vector<std::vector<std::pair<int, int>>> SparseTable(n, 
		std::vector<std::pair<int, int>>(log[n] + 1, std::make_pair(n, n)));
	CountSparseTable(SparseTable, elem, n, log);
	for (size_t i = 0; i < numRequests; i++) {
		size_t l = 0;
		size_t r = 0;
		std::cin >> l >> r;
		--l, --r;
		std::cout << GetAns(l, r, SparseTable, log, elem) << std::endl;
	}
	system("pause");
	return 0;
}