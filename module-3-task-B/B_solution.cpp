#include<algorithm>
#include<iostream>
#include<vector>

struct CEdge {
	int weight;
	int start;
	int finish;
	CEdge(int _weight, int _start, int _finish) :
		start(_start),
		finish(_finish),
		weight(_weight)
	{
	}
};

bool operator<(CEdge e1, CEdge e2) {
	return e1.weight < e2.weight;
}

int MSTWeight(std::vector<CEdge>& edges, int numVertices) {
	int ans = 0;
	std::vector<int> comp(numVertices + 1);
	for (size_t i = 0; i <= numVertices; i++) {
		comp[i] = i;
	}
	std::sort(edges.begin(), edges.end());
	for (auto e : edges) {
		if (comp[e.start] != comp[e.finish]) {
			ans += e.weight;
			int c = comp[e.start];
			for (size_t i = 1; i < numVertices + 1; i++) {
				if (comp[i] == c) {
					comp[i] = comp[e.finish];
				}
			}
		}
	}
	return ans;
}
int main() {
	int numVertices = 0;
	int numEdges = 0;
	std::cin >> numVertices >> numEdges;
	std::vector<CEdge> edges;
	for (size_t i = 0; i < numEdges; i++) {
		int b = 0;
		int e = 0;
		int w = 0;
		std::cin >> b >> e >> w;
		edges.push_back(CEdge(w, b, e));
	}
	std::cout << MSTWeight(edges, numVertices);
	//system("pause");
	return 0;
}