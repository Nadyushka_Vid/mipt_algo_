#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "B.h"


TEST_CASE("Test 1: 1 edge") {
	int numVertices = 2;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(1,2,3));
	edges.push_back(CEdge(2,1,3));

	REQUIRE(MSTWeight(edges, numVertices) == 3);
}
TEST_CASE("Test 2: simple cycle") {
	int numVertices = 3;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(1,2,1));
	edges.push_back(CEdge(2,1,1));
	edges.push_back(CEdge(1,3,1));
	edges.push_back(CEdge(3,1,1));
	edges.push_back(CEdge(2,3,1));
	edges.push_back(CEdge(3,2,1));

	REQUIRE(MSTWeight(edges, numVertices) == 2);
}
TEST_CASE("Test 3: sth else") {
	int numVertices = 3;
	std::vector<CEdge> edges;
	edges.push_back(CEdge(3,1,2));
	edges.push_back(CEdge(3,2,1));
	edges.push_back(CEdge(1,1,2));
	edges.push_back(CEdge(1,2,1));
	edges.push_back(CEdge(2,1,3));
	edges.push_back(CEdge(2,3,1));
	edges.push_back(CEdge(4,2,3));
	edges.push_back(CEdge(4,3,2));

	REQUIRE(MSTWeight(edges, numVertices) == 3);
}