#include<algorithm>
#include<iostream>
#include<map>
#include<set>
#include<string>
#include<vector>

void Dijkstra(std::vector<int>& parent, std::vector<int>& dist,
	std::multimap<std::pair<int, int>, int>& weight, std::vector<std::vector<int>>& graph) {
	std::vector<bool> used(graph.size(), false);
	std::set<std::pair<int, int>> distSort;
	for (int i = 0; i < graph.size(); i++) {
		distSort.insert(std::make_pair(dist[i], i));
	}
	for (int i = 0; i < graph.size(); i++) {
		while (used[(*distSort.begin()).second]) {
			distSort.erase(distSort.begin());
		}
		int v = (*distSort.begin()).second;
		if (dist[v] == 1e7) {
			break;
		}
		distSort.erase(distSort.begin());
		used[v] = true;
		for (auto u : graph[v]) {
			if (dist[v] + (*weight.find(std::make_pair(v, u))).second < dist[u]) {
				dist[u] = dist[v] + (*weight.find(std::make_pair(v, u))).second;
				parent[u] = v;
				distSort.insert(std::make_pair(dist[u], u));
			}
		}
	}
}

bool FindFirstPath(int start, int finish, int numVertices, std::vector<int>& parent, std::vector<int>& dist,
	std::vector<int>& path1, std::multimap<std::pair<int, int>, int>& weight, std::vector<std::vector<int>>& graph, 
	std::multimap<std::pair<int, int>, int>& newWeight) {
	dist[start] = 0;
	Dijkstra(parent, dist, weight, graph);
	if (dist[finish] == 1e7) {
		std::cout << "NO";
		return false;
	}
	int counter = finish;
	while (counter != start) {
		path1.push_back(counter);
		counter = parent[counter];
	}
	path1.push_back(start);
	for (auto i : newWeight) {
		i.second += dist[i.first.first] - dist[i.first.second];
	}
	for (size_t i = path1.size() - 1; i > 0; i--) {
		newWeight.erase(newWeight.find(std::make_pair(path1[i], path1[i - 1])));
		if (newWeight.count(std::make_pair(path1[i - 1], path1[i])) == 0) {
			newWeight.insert(std::make_pair(std::make_pair(path1[i - 1], path1[i]), 0));
		}
	}
	return true;
}

bool FindSecondPath(int start, int finish, int numVertices, std::vector<int>& parent, std::vector<int>& dist,
	std::vector<int>& path2, std::multimap<std::pair<int, int>, int>& newWeight) {
	std::vector<std::vector<int>> newGraph(numVertices);
	for (auto i : newWeight) {
		newGraph[i.first.first].push_back(i.first.second);
	}
	parent.assign(numVertices, -1);
	dist.assign(numVertices, 1e7);
	dist[start] = 0;
	Dijkstra(parent, dist, newWeight, newGraph);
	if (dist[finish] == 1e7) {
		std::cout << "NO";
		return false;
	}
	
	int counter = finish;
	while (counter != start) {
		path2.push_back(counter);
		counter = parent[counter];
	}
	path2.push_back(start);
	return true;
}

void DFS(int v, std::vector<std::vector<int>>& finalGraph, std::vector<bool>& used, 
int finish, int* stop, std::string& ans) {
	ans += to_string(v+1) + " ";
	if (v == finish) {
		*stop = true;
	}
	if (*stop) {
		return;
	}
	used[v] = true;
	for (auto u : finalGraph[v]) {
		if (!used[u]) {
			DFS(u, finalGraph, used, finish, stop);
			if (finalGraph[v].size() > 1) {
				used[v] = false;
			}
			if (stop) {
				return;
			}
		}
	}
}

std::string GetResultPaths(int start, int finish, int numVertices, std::vector<std::vector<int>>& finalGraph) {
	std::string ans = "YES ";
	std::vector<bool> used(numVertices, false);
	int stop = false;
	DFS(start, finalGraph, used, finish, &stop, ans);
	stop = false;
	DFS(start, finalGraph, used, finish, &stop, ans);
    return ans;
}

std::string Solve(int start, int finish, int numVertices, 
	std::multimap<std::pair<int, int>, int>& weight, std::vector<std::vector<int>>& graph) {
	std::vector<int> parent(numVertices, -1);
	std::vector<int> dist(numVertices, 1e7);
	std::vector<int> path1;
	std::multimap<std::pair<int, int>, int> newWeight = weight;
	if (!FindFirstPath(start, finish, numVertices, parent, dist, path1, weight, graph, newWeight)) {
		return "";
	}
	std::vector<int> path2;
	if (!FindSecondPath(start, finish, numVertices, parent, dist, path2, newWeight)) {
		return "";
	}
	std::multimap<std::pair<int, int>, int> finalWeight;
	for (size_t i = path1.size() - 1; i > 0; i--) {
		finalWeight.insert(std::make_pair(std::make_pair(path1[i], path1[i - 1]), 0));
	}
	int reverseEdges = 0;
	for (size_t i = path2.size() - 1; i > 0; i--) {
		if (weight.count(std::make_pair(path2[i], path2[i - 1])) == 0) {
			++reverseEdges;
			finalWeight.erase(finalWeight.find(std::make_pair(path2[i - 1], path2[i])));
		}
		else {
			finalWeight.insert(std::make_pair(std::make_pair(path2[i], path2[i - 1]), 0));
		}
	}
	if (reverseEdges == 0) {
		std::string ans = "YES ";
		for (int i = path1.size() - 1; i >= 0; i--)
		{
            ans += to_string(path1[i] + 1) + " ";
		}
		for (int i = path2.size() - 1; i >= 0; i--)
		{
			ans += to_string(path2[i] + 1) + " ";
		}
		return ans;
	}
	std::vector<std::vector<int>> finalGraph(graph.size());
	for (auto i : finalWeight) {
		finalGraph[i.first.first].push_back(i.first.second);
	}
	return GetResultPaths(start, finish, numVertices, finalGraph);
}