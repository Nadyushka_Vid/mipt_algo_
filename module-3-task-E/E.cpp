#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "E.h"


TEST_CASE("Test 1") {
	int start = 0;
    int finish = 2;
    int numVertices = 3; 
	std::multimap<std::pair<int, int>, int> weight;
    std::vector<std::vector<int>> graph;
    graph[0].push_back(1);
	weight.insert(std::make_pair(std::make_pair(0, 1), 1));
    graph[0].push_back(2);
	weight.insert(std::make_pair(std::make_pair(0, 2), 1));
    graph[1].push_back(2);
	weight.insert(std::make_pair(std::make_pair(1, 2), 1));

	REQUIRE(Solve(start, finish, numVertices, weight, graph) == "YES 1 3 1 2 3 ");
}
TEST_CASE("Test 2") {
	int start = 0;
    int finish = 1;
    int numVertices = 2; 
	std::multimap<std::pair<int, int>, int> weight;
    std::vector<std::vector<int>> graph;
    graph[0].push_back(1);
	weight.insert(std::make_pair(std::make_pair(0, 1), 1));

	REQUIRE(Solve(start, finish, numVertices, weight, graph) == "");
}
TEST_CASE("Test 3") {
	int start = 0;
    int finish = 1;
    int numVertices = 2; 
	std::multimap<std::pair<int, int>, int> weight;
    std::vector<std::vector<int>> graph;
    graph[0].push_back(1);
	weight.insert(std::make_pair(std::make_pair(0, 1), 1));
    graph[0].push_back(1);
	weight.insert(std::make_pair(std::make_pair(0, 1), 1));

	REQUIRE(Solve(start, finish, numVertices, weight, graph) == "YES 1 2 1 2 ");
}