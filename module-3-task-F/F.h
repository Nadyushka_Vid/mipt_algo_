#include<algorithm>
#include<iostream>
#include<string>
#include<vector>

std::string Solve(int numVertices, std::vector<std::vector<int>>& graph) {
	std::vector<std::vector<int>> vertices(numVertices);
	for (int i = 0; i < numVertices; i++) {
		vertices[i].push_back(i);
	}
	std::vector<int> weight(numVertices, 0);
	std::vector<bool> exist(numVertices, true);
	std::vector<bool> seen(numVertices, false);
	int bestCost = 1e7;
	std::vector<int> bestCut;
	for (size_t phase = 0; phase < numVertices - 1; phase++) {
		seen.assign(numVertices, false);
		weight.assign(numVertices, 0);
		for (size_t i = 0, prev; i < numVertices - phase; i++) {
			int cur = -1;
			for (size_t j = 0; j < numVertices; j++) {
				if (exist[j] && !seen[j] && (cur == -1 || weight[j] > weight[cur])) {
					cur = j;
				}
			}
			if (i == numVertices - phase - 1) {
				if (weight[cur] < bestCost) {
					bestCost = weight[cur], bestCut = vertices[cur];
				}
				vertices[prev].insert(vertices[prev].end(), vertices[cur].begin(), vertices[cur].end());
				for (int j = 0; j < numVertices; ++j) {
					graph[j][prev] += graph[cur][j];
					graph[prev][j] += graph[cur][j];
				}
				exist[cur] = false;
			}
			else {
				seen[cur] = true;
				for (int j = 0; j < numVertices; ++j) {
					weight[j] += graph[cur][j];
				}
				prev = cur;
			}
		}
	}
    std::string ans = ""
	seen.assign(numVertices, false);
	for (auto i : bestCut) {
		seen[i] = true;
        ans += to_string(i + 1) + " ";
	}
    return ans;
}