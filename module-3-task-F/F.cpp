#define CATCH_CONFIG_MAIN
#include <catch2\catch.hpp>
#include "F.h"


TEST_CASE("Test 1") {
	int numVertices = 4;
	std::vector<std::string> guests;
    guests.push_back("0111")
    guests.push_back("1001")
    guests.push_back("1001")
    guests.push_back("1110")
	std::vector<std::vector<int>> graph(numVertices, std::vector<int>(numVertices, 0));
	for (size_t i = 0; i < numVertices; i++) {
		for (size_t j = 0; j < numVertices; j++) {
			if (guests[i][j] == '1') {
				graph[i][j] = 1;
			}
		}
	}

	REQUIRE(Solve(numVertices, graph) == "3 ");
}
TEST_CASE("Test 2") {
	int numVertices = 4;
	std::vector<std::string> guests;
    guests.push_back("0100")
    guests.push_back("1000")
    guests.push_back("0001")
    guests.push_back("0010")
	std::vector<std::vector<int>> graph(numVertices, std::vector<int>(numVertices, 0));
	for (size_t i = 0; i < numVertices; i++) {
		for (size_t j = 0; j < numVertices; j++) {
			if (guests[i][j] == '1') {
				graph[i][j] = 1;
			}
		}
	}

	REQUIRE(Solve(numVertices, graph) == "3 4 ");
}
TEST_CASE("Test 3") {
	int numVertices = 5;
	std::vector<std::string> guests;
    guests.push_back("01101")
    guests.push_back("10011")
    guests.push_back("10000")
    guests.push_back("01000")
    guests.push_back("11000")
	std::vector<std::vector<int>> graph(numVertices, std::vector<int>(numVertices, 0));
	for (size_t i = 0; i < numVertices; i++) {
		for (size_t j = 0; j < numVertices; j++) {
			if (guests[i][j] == '1') {
				graph[i][j] = 1;
			}
		}
	}

	REQUIRE(Solve(numVertices, graph) == "4 ");
}